<!DOCTYPE HTML>
<html>
<head>
    <title>Projeto de Loja - Universidade Martins do Varejo</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>

    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/icons/favicon-16x16.png">
    <link rel="manifest" href="assets/img/icons/site.webmanifest">
    <link rel="mask-icon" href="assets/img/icons/safari-pinned-tab.svg" color="#f47200">
    <link rel="shortcut icon" href="assets/img/icons/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Flix Loja">
    <meta name="application-name" content="Flix Loja">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="assets/img/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:image:height" content="254">
    <meta property="og:image:width" content="485">
    <meta property="og:description"
          content="Encontre um projeto pronto para tua loja de forma f&aacute;cil e r&aacute;pida!!">
    <meta property="og:title" content="Flix Loja">
    <meta property="og:url" content="http://projetodeloja.maxmix.net.br">
    <meta property="og:image" content="assets/img/covers/og-image.jpg">

    <link rel="stylesheet" href="assets/css/mainvisualize.css"/>
    <noscript>
        <link rel="stylesheet" href="assets/css/noscript.css"/>
    </noscript>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <link rel="stylesheet" href="assets/css/visualize.css"/>
    <link rel="stylesheet" href="assets/css/layout.css"/>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-102039631-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-102039631-4');
    </script>
</head>
<body class="is-preload">

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div class="logo-flixpreco"><img src="static/img/logo_flixprecos.png" alt="Flix Preços" class=""></div>
            </div>
            <div class="col-sm-0 col-md-4">
                <div class="logo-flix"><img src="static/img/logo_flixdovarejo.png" alt="Flix do Varejo"
                                            class="img-fluid"></div>
            </div>
        </div>
    </div>
</div>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header -->
    <header id="header">
        <div class="inner">

            <!-- Logo -->
            <a href="#" class="logo">
                <span class="title">Escolha sua planta...</span>
            </a>

        </div>
    </header>

    <!-- Main -->
    <div id="main">
        <div class="inner">
            <header>
                <h1>Plantas Encontradas:</h1>
                <p></p>
            </header>
            <section class="tiles">

                <?php

                if (isset($_POST))
                    if (isset($_POST['custom']))
                        print($_POST['custom']);

                ?>

            </section>
        </div>
    </div>

</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

<footer>
    <div class="footer">
        <div class="container">
            <div class="logo-umv"><img src="static/img/logo_umv.png" alt="Universidade Martins do Varejo" class="">
            </div>
        </div>
    </div>
</footer>

</body>
</html>