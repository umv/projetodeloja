function requestWizard() {
    //http://flixadmin.dev.br/api/v1/wizard/formwizard
    $.get('http://umv.maxmix.net.br/api/v1/wizard/formwizard', function (x) {
        $("#wizardform").html(x);
        initForm();
    });
}

function initForm() {
    var form = $("#wizardform #advanced-form").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex)
            {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18)
            {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex)
            {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            //  // Used to skip the "Warning" step if the user is old enough.
            //  if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
            //  {
            //      form.steps("next");
            //  }
            //  // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            //  if (currentIndex === 2 && priorIndex === 3)
            //  {
            //      form.steps("previous");
            //  }
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            //form.submit();
            console.log(1);
            var form = $(this);
            console.log(form);
            console.log(form.attr( "action" ));
            console.log(form.serialize());

            $.ajax({
                method: "POST",
                url: form.attr( "action" ),
                data: form.serialize(),
                })
                .done(function( x ) {
                    // console.log(x);
                    var url = 'visualize.php';
                    var form2 = $('<form action="' + url + '" method="post">' +
                    '<textarea name="custom">' + x + '</textarea>' +
                    '</form>');
                    $('body').append(form2);
                    //console.log(form2);
                    form2.submit();
                });
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        },
        errorLabelContainer: '.errorTxt'
    });
};